package com.coffee.api.Service;


import com.coffee.api.Models.ERole;
import com.coffee.api.Models.Role;
import com.coffee.api.Models.User;
import com.coffee.api.Payload.Request.UserRequest;
import com.coffee.api.Repo.RoleRepo;
import com.coffee.api.Repo.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Transactional
@Slf4j
@Service
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepo roleRepo;

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Page<User> getUsers(String username,   Integer page, Integer size, String reverse, String sort) {
        Sort sortTable = null;
        if (reverse.equals("ASC")) {
            sortTable = Sort.by(sort).ascending();
        }
        if (reverse.equals("DESC")) {
            sortTable = Sort.by(sort).descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortTable);
        return userRepository.findAllByUsernameContains(username,pageable);
    }

    @Override
    public Optional<User> findUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public boolean updateUser(UserRequest newUser, Long id) {

        Set<Role> roles = new HashSet<>();
        if (newUser.getRoles().stream().collect(Collectors.toList()).get(0).equals("user")) {
            Role userRole = roleRepo.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            Role adminRole = roleRepo.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

        }
        userRepository.findById(id)
                .map(u -> {
                    u.setUsername(newUser.getUsername());
                    u.setEmail(newUser.getEmail());
                    u.setPassword(newUser.getPassword());
                    u.setGender(newUser.getGender());
                    u.setPhone(newUser.getPhone());
                    u.setRoles(roles);
                    return userRepository.save(u);
                });
        return true;
    }

    @Override
    public boolean deleteUser(Long id) {
        boolean exists = userRepository.existsById(id);
        if (exists) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteInBatch(List<User> users) {
        try {
            userRepository.deleteInBatch(users);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    public Optional<User> findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public Boolean existsByUsername(String name) {
        return userRepository.existsByUsername(name);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Optional<User> checkExistsAccount(String username, String password) {
        return userRepository.findByUser(username, password);
    }

    @Override
    public Optional<User> getUserByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }
}
