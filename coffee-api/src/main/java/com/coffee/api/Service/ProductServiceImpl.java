package com.coffee.api.Service;

import com.coffee.api.Models.Product;
import com.coffee.api.Models.User;
import com.coffee.api.Repo.ProductRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
// @Slf4j ghi log

@Transactional
//@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepo productRepo;

    @Override
    public Product saveProduct(Product product) {
        return productRepo.save(product);
    }

    @Override
    public Optional<Product> findProductByName(String name) {
        return Optional.ofNullable(productRepo.findByName(name));
    }

    @Override
    public Page<Product> getProducts(String name, Integer page, Integer size, String reverse, String sort) {
        Sort sortTable = null;
        if (reverse.equals("ASC")) {
            sortTable = Sort.by(sort).ascending();
        }
        if (reverse.equals("DESC")) {
            sortTable = Sort.by(sort).descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortTable);
        return productRepo.findAllByNameContains(name, pageable);
    }


    @Override
    public Optional<Product> findProductById(Long id) {
        return productRepo.findById(id);
    }

    @Override
    public boolean updateProduct(Product newProduct, Long id) {
        Optional<Product> product = productRepo.findById(id);
        product.map(pro -> {
            pro.setName(newProduct.getName());
            pro.setAttributes(newProduct.getAttributes());
            pro.setCategory(newProduct.getCategory());
            pro.setImage(newProduct.getImage());
            return productRepo.save(pro);
        }).orElseGet(() -> {
            newProduct.setId(id);
            return productRepo.save(newProduct);
        });
        return true;

    }

    @Override
    public boolean deleteProduct(Long id) {
        boolean product = productRepo.existsById(id);
        if (product) {
            productRepo.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteInBatch(List<Product> products) {
        try {
            productRepo.deleteInBatch(products);
            return true;
        } catch (Exception exception) {
            return false;
        }

    }




//    @Override
//    public Attribute saveAttribute(Attribute attribute) {
//
//        return attributeRepo.save(attribute);
//    }
//
//    @Override
//    public boolean addAttrToProduct(Long productId, Long attrId) {
//        Optional<Product> product = productRepo.findById(productId);
//        Optional<Attribute> attribute = attributeRepo.findById(attrId);
//
//        if (product.isPresent() && attribute.isPresent()) {
//            product.get().getAttributes().add(attribute.get());
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public Product getUser(Long id) {
//        return productRepo.getById(id);
//    }
}
