package com.coffee.api.Service;

import com.coffee.api.Models.User;
import com.coffee.api.Payload.Request.UserRequest;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {
    User saveUser(User user);
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String name);
    Page<User> getUsers(String username,   Integer  page, Integer size, String reverse, String sort);
    Optional<User> findUserById(Long id);
    boolean updateUser(UserRequest user, Long id);
    boolean deleteUser(Long id);
    boolean deleteInBatch(List<User> users);
    Optional<User> findByUsername(String name);
    Boolean existsByUsername(String name);
    Boolean existsByEmail(String email);
    Optional<User> checkExistsAccount(String username,String password);
    Optional<User> getUserByUserName(String userName);
}
