package com.coffee.api.Service;

import com.coffee.api.Models.Product;
import com.coffee.api.Models.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface ProductService {
    Product saveProduct(Product product);
    Optional<Product> findProductByName(String name);
    Page<Product> getProducts(String name, Integer page, Integer size, String reverse, String sort );
    Optional<Product> findProductById(Long id);
    boolean updateProduct(Product product,Long id);
    boolean deleteProduct(Long id);
    boolean deleteInBatch(List<Product> products);

}

//    Attribute saveAttribute(Attribute attribute);
//
//    boolean addAttrToProduct(Long productId, Long attrId);
//
//    Product getUser(Long id);