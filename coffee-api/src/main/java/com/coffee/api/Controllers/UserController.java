package com.coffee.api.Controllers;

import com.coffee.api.Models.ERole;
import com.coffee.api.Models.ResponseObject;
import com.coffee.api.Models.Role;
import com.coffee.api.Models.User;
import com.coffee.api.Payload.Request.LoginRequest;
import com.coffee.api.Payload.Request.UserRequest;
import com.coffee.api.Repo.RoleRepo;
import com.coffee.api.Repo.UserRepository;
import com.coffee.api.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserController {
    //@Autowire only use when one depentenci
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepo roleRepo;

    @GetMapping("/test")
    public ResponseEntity<ResponseObject> test() {
        //List<User> users=userRepository.findAllActiveUsers();
        List<User> users = userRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "List user", users));
    }

    //    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("list")
    public ResponseEntity<ResponseObject> getListUser(
            @RequestParam(name = "username", required = false) String username,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(name = "reverse", required = false, defaultValue = "ASC") String reverse,
            @RequestParam(name = "sort", required = false, defaultValue = "email") String sort
    ) {
        Page<User> users = userService.getUsers(username, page, size, reverse, sort);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "List user", users));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseObject> getUserById(@PathVariable Long id) {
        Optional<User> user = userService.findById(id);
        return user.isPresent() ? ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200, "success", user))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseObject(400, "Cannot find product id=" + id, null));
    }

//    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("insert")
    public ResponseEntity<ResponseObject> insertUser( @RequestBody UserRequest user) {

        if (userService.existsByUsername(user.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseObject(400, "Username is already taken", ""));
        }
        if (userService.existsByEmail(user.getEmail())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ResponseObject(400, "Email is already in use!", ""));
        }

        User newUser = new User(user.getUsername(),user.getGender(),
                user.getPhone(),user.getEmail(),
                encoder.encode(user.getPassword()));

        Set<Role> roles=new HashSet<>();
        if (user.getRoles().stream().collect(Collectors.toList()).get(0).equals("user")) {
            Role userRole = roleRepo.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            Role adminRole = roleRepo.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

        }
        newUser.setRoles(roles);
        userService.saveUser(newUser);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "insert user success",null));

    }

    @PutMapping("update/{id}")
    public ResponseEntity<ResponseObject> updateUser(@Valid @RequestBody UserRequest newUser, @PathVariable long id) {
        userService.updateUser(newUser, id);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject(200, "update user success", newUser));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ResponseObject> deleteUserById(@PathVariable Long id) {
        boolean result = userService.deleteUser(id);
        return result
                ? ResponseEntity.status(HttpStatus.OK).body(new ResponseObject(200, "Delete product success", ""))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject(400, "Cannot find user with id =" + id, ""));
    }


    @CrossOrigin("http://localhost:4200/")
    @PostMapping("/deletes")
    public ResponseEntity<ResponseObject> deleteProduct(@Valid @RequestBody List<User> users) {
        boolean result = userService.deleteInBatch(users);
        return result ? ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "delete users success", true))
                : ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                .body(new ResponseObject(400, "Failed take delete users", false));

    }

}
