package com.coffee.api.Controllers;


import com.coffee.api.Models.ERole;
import com.coffee.api.Models.ResponseObject;
import com.coffee.api.Models.Role;
import com.coffee.api.Models.User;
import com.coffee.api.Payload.Request.LoginRequest;
import com.coffee.api.Payload.Request.SignupRequest;
import com.coffee.api.Payload.Response.JwtResponse;
import com.coffee.api.Payload.Response.MessageResponse;
import com.coffee.api.Repo.RoleRepo;
import com.coffee.api.Security.Jwt.JwtUtils;
import com.coffee.api.Security.SercurityService.UserDetailsImpl;
import com.coffee.api.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    UserService userService;
    @Autowired
    RoleRepo roleRepository;
    @Autowired
    JwtUtils jwtUtils;


    //UsernamePasswordAuthenticationToken get username and password,
    //authenticationManager will use it to authenticate a login account
    // If successful, AuthenticationManager returns a fully populated Authentication object (including granted authorities).
    // set the current UserDetails in SecurityContext using setAuthentication(authentication) method.
    //After this, everytime you want to get UserDetails, just use SecurityContext like this:
    //UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    @PostMapping("/test")
    public ResponseEntity<?> test(@Valid @RequestBody LoginRequest loginRequest) {

        Optional<User> user = userService.checkExistsAccount(loginRequest.getUsername(), loginRequest.getPassword());
        return ResponseEntity
                .badRequest()
                .body(new ResponseObject(403, "User not exists", user));
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Optional<User> user = userService.getUserByUserName(loginRequest.getUsername());
        PasswordEncoder passEncoder = new BCryptPasswordEncoder();
        if (user.isPresent()) {
            if (passEncoder.matches(loginRequest.getPassword(), user.get().getPassword())) {
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
                // save to SecurityContextHolder, after use it, only to call SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                // we will get info userDetail
                SecurityContextHolder.getContext().setAuthentication(authentication);
                String jwt = jwtUtils.generateJwtToken(authentication);//genarl token

                UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();// covert userDetail to UserDetailsImpl
                List<String> roles = userDetails.getAuthorities().stream()// covert role to string[]
                        .map(item -> item.getAuthority())
                        .collect(Collectors.toList());
                return ResponseEntity.ok(new JwtResponse(jwt,
                        userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(),
                        roles));
            }
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseObject(400, "password not match", null));

        } else {
            return ResponseEntity
                    .badRequest()
                    .body(new ResponseObject(400, "Username not exists", null));
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        // Create new user's account with username,email, password and roles.
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);
                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        userService.saveUser(user);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
