package com.coffee.api.Controllers;


import com.coffee.api.Models.Product;
import com.coffee.api.Models.ResponseObject;
import com.coffee.api.Service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RequiredArgsConstructor// not use Autowired
@RestController
@Validated
@CrossOrigin("http://localhost:4200/")
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;


    @GetMapping("list")
    public ResponseEntity<ResponseObject> getAllProduct(
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(name = "reverse", required = false, defaultValue = "ASC") String reverse,
            @RequestParam(name = "sort", required = false, defaultValue = "name") String sort
    ) {

        Page<Product> products = productService.getProducts(name, page, size, reverse, sort);

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "List product", products));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseObject> getProductById(@PathVariable Long id) {
        Optional<Product> product = productService.findProductById(id);
        return product.isPresent()
                ? ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "product by id" + id, product))
                : ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                .body(new ResponseObject(200, "Empty", null));
    }

    @PostMapping("insert")
    public ResponseEntity<ResponseObject> insertProduct(@Valid @RequestBody Product product) {

        Optional<Product> prod = productService.findProductByName(product.getName());
        return !prod.isPresent() ? ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "insert product success", productService.saveProduct(product)))
                : ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseObject(400, "product already taken", ""));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<ResponseObject> updateProduct(@Valid @RequestBody Product product, @PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "update product success", productService.updateProduct(product, id)));
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<ResponseObject> deleteProduct(@PathVariable Long id) {
        boolean result = productService.deleteProduct(id);
        return result ? ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "delete product success", true))
                : ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                .body(new ResponseObject(400, "Failed take delete product", false));
    }

    @CrossOrigin("http://localhost:4200/")
    @PostMapping("/deletes")
    public ResponseEntity<ResponseObject> deleteProduct(@Valid @RequestBody List<Product> products) {

        boolean result = productService.deleteInBatch(products);
        return result ? ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject(200, "delete products success", true))
                : ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                .body(new ResponseObject(400, "Failed take delete product", false));

    }


//    @PostMapping("insert/attribute")
//    public ResponseEntity<ResponseObject> insertAttribute(@RequestBody Attribute attribute) {
//        return ResponseEntity.status(HttpStatus.OK)
//                .body(new ResponseObject(200, "insert product success", productService.saveAttribute(attribute)));
//
//    }
//
//    @PostMapping("insert/addAttributeToProduct")
//    public ResponseEntity<ResponseObject> addAttributeToProduct(@RequestBody AttributeAndProduct form) {
//        boolean result = productService.addAttrToProduct(form.getProductId(), form.getAttributeId());
//        return result ? ResponseEntity.status(HttpStatus.OK)
//                .body(new ResponseObject(200, "insert product success", ""))
//                : ResponseEntity.status(HttpStatus.OK)
//                .body(new ResponseObject(400, "Failed for add attribute to product", ""));
//
//    }

}

//@Data
//class AttributeAndProduct {
//    private Long productId;
//    private Long attributeId;
//}
