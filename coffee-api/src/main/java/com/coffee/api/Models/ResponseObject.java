package com.coffee.api.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @AllArgsConstructor

public class ResponseObject {

    @Getter @Setter
    private Integer status;
    @Getter @Setter
    private String messages;
    @Getter @Setter
    private Object data;


}
