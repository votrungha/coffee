package com.coffee.api.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "product")
@NoArgsConstructor
@AllArgsConstructor
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @NotBlank(message = "Name  is required ")
    private String name;

    @NotBlank(message = "Image  is required ")
    private String image;

    @NotBlank(message = "Category  is required ")
    private String category;

    @NotBlank(message = "description  is required ")
    private String description;


    //cách 1: tự động ánh xạ
//    orphanRemoval = true nếu attributes= null thì sẽ báo lỗi
//FetchType.LAZY object product not load, only when you call it
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "product_id")
    private Set<Attribute> attributes = new HashSet<>();


    //cách 2: cần ánh xạ id cho bảng attribute

//    @OneToMany(
//            mappedBy = "product",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true
//    )
//    List<Attribute> attributes =new ArrayList<>();


//    public Product(String name, String image, String category, String description) {
//        this.name = name;
//        this.image = image;
//        this.category = category;
//        this.description = description;
//    }

    public Product(String name, String image, String category, String description, Set<Attribute> attributes) {
        this.name = name;
        this.image = image;
        this.category = category;
        this.description = description;
        this.attributes = attributes;
    }

    // khắc phục attributes= null thì sẽ báo lỗi
    public void setAttributes(Set<Attribute> attributes) {
        if (this.attributes == null) {
            this.attributes = attributes;
        } else {
            this.attributes.retainAll(attributes);
            this.attributes.addAll(attributes);
        }
    }
}
/*Serialization  chuyển đổi trạng thái của một Java object thành một định dạng nào đó
để Java object này có thể được lưu trữ ở đâu đó và sau đó, nó sẽ được sử dụng bởi một tiến trình khác , thường được chuyển về kiểu byte stream*/