package com.coffee.api.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "Attribute")
@NoArgsConstructor @AllArgsConstructor()
public class Attribute {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @NotBlank(message = "Size  is required ")
    private String Size;

    @NotNull(message = "price  is required ")
    private Double price;

    // cách 2: ánh xạ nhận id product làm khóa phụ

//    @ManyToOne
//    @JoinColumn(name = "product_id")
//    @JsonIgnore
//    private Product product;


    public Attribute(String size, Double price) {
        Size = size;
        this.price = price;
    }
}


//FetchType.LAZY object product not load, only when you call it
//
//    @ManyToOne
//    @JoinColumn(name = "product_id")
//    private Product product;

//    public Attribute(String size, Double price, Product product) {
//        Size = size;
//        this.price = price;
//        this.product = product;
//    }
