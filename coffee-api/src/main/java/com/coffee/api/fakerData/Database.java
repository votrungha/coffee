package com.coffee.api.fakerData;

import com.coffee.api.Models.*;
import com.coffee.api.Repo.AttributeRepo;
import com.coffee.api.Repo.ProductRepo;
import com.coffee.api.Repo.RoleRepo;
import com.coffee.api.Repo.UserRepository;
import com.coffee.api.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class Database {

    @Autowired
    PasswordEncoder encoder;

    @Bean
    CommandLineRunner commandLineRunner(ProductService productService, AttributeRepo attributeRepo, ProductRepo productRepo, UserRepository userRepository, RoleRepo roleRepo) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {

                if (productRepo.findAll().isEmpty()) {
                    Attribute a1 = new Attribute("M", 15.000);
                    Set<Attribute> attributes = new HashSet<>();
                    attributes.add(a1);
                    Product p = new Product("Xá xị",
                            "https://cdn.fast.vn/tmp/20200919065358-nuoc-ngot-fanta-huong-xa-xi-330ml-1.jpg",
                            "Giải khát", "Nước bão hòa CO2, đường HFCS, hương xá xị tổng hợp," +
                            " màu thực phẩm (E150d), chất điều chỉnh độ axit (E330)", attributes);
                    productService.saveProduct(p);
                    Attribute a = new Attribute("M", 15.000);
                    Attribute a2 = new Attribute("L", 17.000);
                    Set<Attribute> attribute1 = new HashSet<>();
                    attribute1.add(a);
                    attribute1.add(a2);
                    Product p1 = new Product("Cà phê",
                            "https://img.thuthuatphanmem.vn/uploads/2018/10/04/anh-dep-ben-ly-cafe-den_110730392.jpg",
                            "Cà phê", "Cà phê được các nhà khoa học và nhà nghiên cứu dinh dưỡng đánh giá rất cao về tác dụng của chúng đối với sức khỏe."
                            , attribute1);
                    productService.saveProduct(p1);

                    Attribute a4 = new Attribute("M", 15.000);
                    Attribute a5 = new Attribute("L", 17.000);
                    Set<Attribute> attribute2 = new HashSet<>();
                    attribute2.add(a4);
                    attribute2.add(a5);
                    Product p4 = new Product("Dưa hấu",
                            "https://ameovat.com/wp-content/uploads/2016/07/cach-lam-sinh-to-dua-hau-3-600x454.jpg",
                            "Sinh tố", "Cách làm sinh tố dưa hấu thơm ngon bổ dưỡng – Cach lam sinh to dua hau"
                            , attribute2);
                    productService.saveProduct(p4);

                    Attribute a3 = new Attribute("M", 15.000);
                    Set<Attribute> attribute3 = new HashSet<>();
                    attribute3.add(a3);
                    Product p3 = new Product("Tăng lực",
                            "https://dailynuoc.com/thumb.php?src=https://dailynuoc.com/img_data/images/thung-24-chai-nuoc-tang-luc-number1-330ml012982766531.jpg&w=600&h=450&zc=2",
                            "Giải khát", "Nước Tăng Lực Number 1 được tạo ra từ sự kết hợp của Vitamin B3, Taurine, Inositol và Caffeine để giúp người dùng nạp nhanh năng lượng"
                            , attribute3);
                    productService.saveProduct(p3);

                    Attribute a6 = new Attribute("M", 15.000);
                    Attribute a7 = new Attribute("XL", 20.000);
                    Set<Attribute> attribute4 = new HashSet<>();
                    attribute4.add(a6);
                    attribute4.add(a7);
                    Product p5 = new Product("Bí đao",
                            "http://gongcha.com.vn/wp-content/uploads/2018/02/Trà-Bí-Đao-Alisan-2.png",
                            "Giải khát", "Trà bí đao ngọt ngào kết hợp với trà Alisan, cho ra ly trà thơm, ngọt dịu dễ uống.g"
                            , attribute4);
                    productService.saveProduct(p5);

                    Attribute a8 = new Attribute("M", 15.000);
                    Attribute a9 = new Attribute("XL", 20.000);
                    Set<Attribute> attribute5 = new HashSet<>();
                    attribute5.add(a8);
                    attribute5.add(a9);
                    Product p6 = new Product("Siro",
                            "https://yt.cdnxbvn.com/medias/uploads/195/195444-cach-lam-siro-dau.jpg",
                            "Giải khát", "Trong các cách làm siro dâu, siro ngâm đường là cách dễ nhất. Đây cũng là cách thực hiện không tốn công."
                            , attribute5);
                    productService.saveProduct(p6);

                    Attribute a10 = new Attribute("M", 15.000);
                    Attribute a11 = new Attribute("XL", 20.000);
                    Set<Attribute> attribute7 = new HashSet<>();
                    attribute7.add(a10);
                    attribute7.add(a11);
                    Product p7 = new Product("Cà rốt",
                            "https://img.thuthuatphanmem.vn/uploads/2018/10/04/anh-dep-ben-ly-cafe-den_110730392.jpg",
                            "Sinh tố", "Ho đờm là một triệu chứng phổ biến khi chúng bị cảm lạnh và một số căn bệnh khác, có tác động không hề nhỏ đối với hệ hô hấp."
                            , attribute7);
                    productService.saveProduct(p7);
                }


                if (userRepository.findAll().isEmpty()) {
                    Role role = new Role(ERole.ROLE_USER);
                    Role role1 = new Role(ERole.ROLE_ADMIN);
                    roleRepo.save(role);
                    roleRepo.save(role1);
                    Set<Role> userRole = new HashSet<>();
                    userRole.add(role);
                    Set<Role> adminRole = new HashSet<>();
                    adminRole.add(role1);

                    User admin = new User("admin",
                            "admin@gmail.com",
                            encoder.encode("123456"));
                    admin.setRoles(adminRole);
                    userRepository.save(admin);

                    User user = new User("user",
                            "user@gmail.com",
                            encoder.encode("123456"));
                    user.setRoles(userRole);
                    userRepository.save(user);

                }

            }
        }

                ;
    }
}
