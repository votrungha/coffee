package com.coffee.api.Repo;

import com.coffee.api.Models.ERole;
import com.coffee.api.Models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
