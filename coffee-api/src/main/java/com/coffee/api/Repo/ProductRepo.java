package com.coffee.api.Repo;


import com.coffee.api.Models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepo  extends JpaRepository<Product,Long> {

    Product findByName(String name);
    Page<Product> findAllByNameContains(String name, Pageable pageable);

    // example update with customer Query

//    @Modifying
//    @Query("Update Todo t SET t.title=:title WHERE t.id=:id")
//    public void updateTitle(@Param("id") Long id, @Param("title") String title);
}
