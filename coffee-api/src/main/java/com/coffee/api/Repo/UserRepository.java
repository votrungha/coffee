package com.coffee.api.Repo;

import com.coffee.api.Models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUsername(String name);
    Boolean existsByUsername(String name);
    Boolean existsByEmail(String email);
    Optional<User> findByEmail(String email);
    Page<User> findAllByUsernameContains(String userName, Pageable pageable);
    @Query("SELECT u FROM User u WHERE u.username = ?1 and u.password= ?2")
    Optional<User> findByUser(String username,String password);
//    @Query
//    @Query("SELECT u FROM User u WHERE u.id = 420")
//    @Query(
//            value = "SELECT * FROM USER u WHERE u.id = 420",
//            nativeQuery = true)
//    List<User> findAllActiveUsers();
}
