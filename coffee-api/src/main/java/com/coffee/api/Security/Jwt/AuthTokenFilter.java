package com.coffee.api.Security.Jwt;

import com.coffee.api.Security.SercurityService.UserDetailServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*khi gửi 1 yêu cầu tới api AuthTokenFilter sẽ được run, để lấy jwt trên header request,
 * sau đó tiến hành validator
 * */
// OncePerRequestFilter makes a single execution for each request to our API
/*
 *  method that we will implement parsing & validating JWT,
 * loading User details (using UserDetailsService),
 * checking Authorizaion (using UsernamePasswordAuthenticationToken).
 * */
@Slf4j
public class AuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);// we must give request to parseJwt() method, for to get token form user give to server.
            // check jwt have difference null and moved on it to validateJwtToken() method to check with secret.
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {// if condition is true
                String username = jwtUtils.getUserNameFromJwtToken(jwt);// moved jwt to getUserNameFromJwtToken() method for get username;
                //after that we use username to build userDetail.
                UserDetails userDetails = userDetailService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                //WebAuthenticationDetailsSource  convert an instance of HttpServletRequest class into an instance of the WebAuthenticationDetails class.
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }

        } catch (Exception ex) {
            logger.error("Cannot set user authentication: {}", ex);
        }
        filterChain.doFilter(request, response);

    }


    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }
        return null;
    }
}
