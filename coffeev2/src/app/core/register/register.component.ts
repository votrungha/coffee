import { Component, OnInit } from '@angular/core';
import { ACCOUNT } from 'src/app/entity/accounts/account.model';
import { AccountService } from 'src/app/entity/accounts/account.service';
import { AccountServiceService } from '../auth/account-service.service';
import { Auth } from '../guard.model';
import { ToastrService } from 'ngx-toastr';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  Accounts: ACCOUNT[] = [];
  account: Auth = {
    email: '',
    password: '',
    username: '',
  };
  isValidator: boolean = false;

  constructor(
    private authService: AccountServiceService,
    private toast: ToastrService,
  
  ) {}

  ngOnInit(): void {}
  setIsvalidator() {
    this.isValidator = !this.isValidator;
  }

  
  register() {
    this.authService.Register(this.account).subscribe(
      (result) => {
        console.log(result);
        this.toast.success('Thành công', 'Tạo tài khoản!');
      },
      (err) => {
        const erros = err.error.data;
        erros.forEach((er: any) => this.toast.error('Thất bại', er));
      }
    );
  }
}
