import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { ACCOUNT } from "src/app/entity/accounts/account.model";
import { Auth } from "../guard.model";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";
import { ServiceService } from "src/app/service/service.service";
import { Router } from "@angular/router";
import { SERVER_API_URL } from '../../constant';
export interface MESSAGE {
  message: string;
}

@Injectable({
  providedIn: "root",
})
export class AccountServiceService {
  private error = new Subject<any>();
  public error$ = this.error.asObservable();
  constructor(
    private http: HttpClient,
    private $locationStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private publicService: ServiceService,
    private router: Router
  ) {}

  optionHeader = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  sendError(error: string) {
    this.error.next(error);
  }
  

  Login(account: Auth): Observable<any> {
    return this.http.post(
      SERVER_API_URL + `auth/signin`,
      account,
      this.optionHeader
    );
  }

  authenticateSuccess(bearerToken: string): void {
    if (bearerToken) {
      this.$locationStorage.store("authenticationtoken", bearerToken);
      return;
    }
  }
  
  Register(account: Auth): Observable<any> {
    return this.http.post(
      SERVER_API_URL + `auth/signup`,
      account,
      this.optionHeader
    );
  }


  logout() {
    //remove our servise
    this.$locationStorage.clear("authenticationtoken");
    this.$sessionStorage.clear();
    this.router.navigate(["/login"]);
    sessionStorage.clear();
    localStorage.clear();
    window.location.reload();
  }
}
