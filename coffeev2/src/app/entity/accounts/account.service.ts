import { Injectable } from '@angular/core';
import { ACCOUNT } from './account.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, tap } from 'rxjs';
import { SERVER_API_URL } from '../../constant';
@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private http: HttpClient) {}
  optionHeader = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getAccounts(req: any): Observable<any> {
    return this.http.get(
      SERVER_API_URL +
        'v1/user/list?username=' +
        req.username + 
        '&page=' +
        req.current_page +
        '&size=' +
        req.limit +
        '&sort=' +
        req.sort +
        '&reverse=' +
        req.reverse
    );
  }

  addAccount(account: ACCOUNT): Observable<any> {
    console.log(account);
    return this.http.post<ACCOUNT>(
      SERVER_API_URL + 'v1/user/insert',
      account,
      this.optionHeader
    );
  }

  updateAccount(account: ACCOUNT, id: Number): Observable<ACCOUNT[]> {
 
    return this.http.put<ACCOUNT[]>(
      SERVER_API_URL + `v1/user/update/${id}`,
      account,
      this.optionHeader
    );
    // .pipe(
    //   tap((_) => console.log(`updated account id=${account.id}`)),
    //   catchError(this.handleError<any>('updateAccount'))
    // );
  }

  deleteAccount(id: string): Observable<ACCOUNT> {
    return this.http.delete<ACCOUNT>(
      SERVER_API_URL + `v1/user/delete/` + id,
      this.optionHeader
    );
  }

  deleteAccounts(account: ACCOUNT[]): Observable<ACCOUNT> {
    return this.http.post<ACCOUNT>(
      SERVER_API_URL + `v1/user/deletes`,
      account,
      this.optionHeader
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
