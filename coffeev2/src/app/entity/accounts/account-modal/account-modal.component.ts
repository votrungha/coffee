import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ACCOUNT } from '../account.model';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.scss'],
})
export class AccountModalComponent implements OnInit {
  AccountForm!: FormGroup;
  ROLES: Array<string> = ["user", "admin"];
  isValidator: boolean = false;
 
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { title: string; account: ACCOUNT },
    private dialogRef: MatDialogRef<AccountModalComponent>,
    private accountService: AccountService
  ) {
    this.AccountForm = new FormGroup({
      id: new FormControl(''),
      username: new FormControl('', [Validators.required]),
      gender: new FormControl('Nam', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required]),
      roles: new FormControl("user", [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }
  ngOnInit(): void {
    if (this.data.account  ) {
      
        this.userName?.setValue(this.data.account.username);
        this.id?.setValue(this.data.account.id);
        this.roles?.setValue(this.data.account.roles[0]?.name.slice(5).toLowerCase());
        this.phone?.setValue(this.data.account.phone);
        this.gender?.setValue(this.data.account.gender);
        this.email?.setValue(this.data.account.email);
        this.password?.setValue(this.data.account.password);
    }
  }

  get userName() {
    return this.AccountForm.get('username');
  }
  get id() {
    return this.AccountForm.get('id');
  }
  get phone() {
    return this.AccountForm.get('phone');
  }
  get roles() {
    return this.AccountForm.get("roles");
  }
  get gender() {
    return this.AccountForm.get('gender');
  }
  get email() {
    return this.AccountForm.get('email');
  }
  get password() {
    return this.AccountForm.get('password');
  }

  setValidator() {
    this.isValidator = true;
  }
  save() {
    this.dialogRef.close(this.AccountForm.value);
  }
  close() {
    this.dialogRef.close(null);
  }
}
