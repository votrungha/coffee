import { Component, OnInit } from '@angular/core';
import { ACCOUNT, CONTENTS_LIST_WIDTH } from './account.model';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AccountService } from './account.service';
import { ToastrService } from 'ngx-toastr';
import { AccountModalComponent } from './account-modal/account-modal.component';
import { ServiceService } from 'src/app/service/service.service';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { CONTENTS_LIST_HEADER_ACC } from 'src/app/constant';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  Accounts: ACCOUNT[] = [];
  Account!: ACCOUNT;
  CONTENTS_LIST_WIDTH: CONTENTS_LIST_WIDTH[] = CONTENTS_LIST_HEADER_ACC;

  params: any = {
    current_page: 0,
    limit: 5,
    sort: 'email',
    reverse: 'ASC',
    total_records: 0,
    delete_all: false,
    username: '',
  };
  start = 0;
  end = 5;

  constructor(
    private accountService: AccountService,
    public dialog: MatDialog,
    private servicePublic: ServiceService,
    private toastr: ToastrService,
    private loadService: NgxUiLoaderService
  ) {}

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll() {
    this.loadService.start();
    this.accountService.getAccounts(this.params).subscribe((res) => {
      this.Accounts = res.data.content || [];
      this.params.total_records = res.data.totalElements;
    });
    this.loadService.stop();
  }
  paramsChange(e: any) {
    this.findStart();
    this.findEnd();
    this.loadAll();
  }

  handlDeleteMultiple() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Xóa tài khoản',
      message: 'Bạn có muốn xóa tài những khoản này không?',
    };
    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      const accounts: ACCOUNT[] = [];
      if (data) {
        this.loadService.start();
        this.Accounts.map((account) => {
          if (account?.completed) {
            accounts.push(account);
          }
        });
        this.accountService.deleteAccounts(accounts).subscribe(
          (res) => {
            this.loadAll();
            this.toastr.success('Thành công!', 'Xóa người dùng');
          },
          (error) => this.toastr.error('Thất bại', error.error.messages)
        );

        this.params.delete_all = false;
        this.loadService.stop();
      }
    });
  }

  findStart() {
    this.start = (this.params.current_page - 1) * this.params.limit;
  }
  findEnd() {
    const result = this.params.current_page * this.params.limit;
    result > this.params.total_records
      ? (this.end = this.params.total_records)
      : (this.end = result);
  }
  sortAll(value: any) {
    this.params.current_page = 0;
    this.params.sort = value.sort;
    this.params.reverse = value.reverse;
    this.loadAll();
  }

  handlSearch(value: any) {
    this.loadService.start();
    this.params.current_page = 0; 
    this.params.username=value;
    this.loadAll();
    this.loadService.stop();
  }

  handlDeleteAccount(id: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Xóa tài khoản',
      message: 'Bạn có muốn xóa tài khoản này không?',
    };
    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.loadService.start();
        this.accountService.deleteAccount(id).subscribe(
          (result) => {
            this.toastr.success('Thành công!', 'Xóa người dùng');
            this.loadAll();
            this.loadService.stop();
          },
          (err) => {
            this.toastr.error('Thất bại!', err.error.message);
          }
        );
      }
    });
  }

  openDialog(account: ACCOUNT) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true; //user will able to close the dialog just by click outside
    dialogConfig.autoFocus = true; // meaning that the focus will be set automatically on the first form filde
    if (account) {
      dialogConfig.data = { title: 'Updated account', account };
    } else {
      dialogConfig.data = { title: 'Created account', account: null };
    }

    const dialogRef = this.dialog.open(AccountModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        const newAccount: ACCOUNT = {
          email: data.email,
          username: data.username,
          gender: data.gender,
          password: data.password,
          phone: data.phone,
          roles: [data.roles],
        };

        if (account) {
          this.loadService.start();
          this.accountService.updateAccount(newAccount, data.id).subscribe(
            (result) => {
              this.toastr.success('Thành công!', 'Cập nhật người dùng');
              this.loadAll();
              this.loadService.stop();
            },
            (error) => console.log(error)
          );
        } else {
          this.loadService.start();
          this.accountService.addAccount(newAccount).subscribe(
            (result) => {
              this.toastr.success('Thành công!', 'Tạo người dùng!');
              this.loadAll();
              this.loadService.stop();
            },
            (error) => {
              this.toastr.error('Thất bại!', error.error.messages);
            }
          );
        }
      }
    });
  }
}
