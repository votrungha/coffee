export interface ACCOUNT {
  id?: string;
  email: string;
  avatar?: string;
  username: string;
  gender: string;
  password: string;
  phone: string;
  roles: Array<ROLE>;
  completed?:boolean;
}

export interface CONTENTS_LIST_WIDTH {
  name: string;
  width: string;
  key?: string;
}

export interface ROLE {
  name:string,
  id:number
}


export interface CONTENTS_LIST_WIDTH1 {
  name: string;
  width: string;
  key?: string;
}
