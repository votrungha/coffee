import { Component, OnInit } from '@angular/core';
import { CONTENTS_LIST_HEADER_PRO } from 'src/app/constant';
import { CONTENTS_LIST_WIDTH, PRODUCT } from './coffee.model';
import { CoffeeService } from './coffee.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { ToastrService } from 'ngx-toastr';
import { ServiceService } from 'src/app/service/service.service';
import { CoffeeModalComponent } from './coffee-modal/coffee-modal.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-coffees',
  templateUrl: './coffees.component.html',
  styleUrls: ['./coffees.component.scss'],
})
export class CoffeesComponent implements OnInit {
  Products: PRODUCT[] = [];
  CONTENTS_LIST_WIDTH: CONTENTS_LIST_WIDTH[] = CONTENTS_LIST_HEADER_PRO;
  params: any = {
    current_page: 0,
    limit: 5,
    sort: 'name',
    reverse: 'ASC',
    total_records: 0,
    delete_all: false,
    name: '',
  };
  start = 0;
  end = 5;
  is: boolean = true;

  constructor(
    private coffeeService: CoffeeService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private servicePublic: ServiceService,
    private loadService: NgxUiLoaderService
  ) {}
  loadAll() {
    this.loadService.start();
    this.coffeeService.getProduct(this.params).subscribe((res) => {
      this.Products = res.body.data.content;
      this.Products.map((item: any, index: number) => {
        var size = '';
        item.attributes.map((att: any, index: number) => {
          size += att.size + ' ';
        });
        item.size = size;
      });
      this.params.total_records = res.body.data.totalElements;
    });
    this.loadService.stop();
  }

  handlDeleteMultiple() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Xóa tài khoản',
      message: 'Bạn có muốn xóa tài những khoản này không?',
    };
    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.loadService.start();
        const deleteProduct: PRODUCT[] = [];
        this.Products.map(async (account) => {
          if (account?.completed) {
            deleteProduct.push(account);
          }
        });
        this.coffeeService
          .deleteAllProducts(deleteProduct)
          .subscribe((result) => {
            this.params.delete_all = false;
            this.loadAll();
            this.toastr.success('Thành công!', 'Xóa người dùng');
          });

        this.loadService.stop();
      }
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }
  sortAll(value: any) {
    this.loadService.start();
    this.params.current_page = 0;
    this.params.sort = value.sort;
    this.loadAll();
    this.loadService.stop();
  }

  async openDialog(product: PRODUCT) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true; //user will able to close the dialog just by click outside
    dialogConfig.autoFocus = true; // meaning that the focus will be set automatically on the first form filde
    if (product) {
      dialogConfig.data = { title: 'Updated Product', product };
    } else {
      dialogConfig.data = { title: 'Created Product', data: null };
    }

    const dialogRef = await this.dialog.open(
      CoffeeModalComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((data) => {
      if (product && data) {
        const newProduct = {
          name: data.name,
          image: data.image,
          category: data.category,
          attributes: data.attributes,
          description: data.descript,
        };
        this.loadService.start();
        this.coffeeService.updateProduct(newProduct, data.id).subscribe(
          (result) => {
            this.toastr.success('Thành công!', 'Cập nhật sản phẩm');
            this.loadAll();
          },
          (error) => {
            console.log(error);
            this.toastr.success('Thất bại!', error.error.message);
          }
        );
        this.loadService.stop();
      }
      if (data && !product) {
        this.loadService.start();
        const newProduct = {
          name: data.name,
          image: data.image,
          category: data.category,
          attributes: data.attributes,
          description: data.descript,
        };
        this.coffeeService.addProduct(newProduct).subscribe(
          (result) => {
            this.toastr.success('Thành công!', 'Tạo sản phẩm!');
          },
          (error) => {
            this.toastr.error('Thất bại!', error.error.messages);
          }
        );

        this.loadAll();
        this.loadService.stop();
      }
    });
  }
  handlSearch(value: any) {
    this.loadService.start();
    this.params.current_page = 0;
    this.coffeeService.getProduct(this.params).subscribe((res) => {
      this.Products = res.body.data.content;
      this.Products = this.Products.filter((item: PRODUCT, index: number) => {
        var size = '';
        item.attributes.map((att, index) => {
          size += att.size + ' ';
        });
        item.size = size;
        return (
          item.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
          item.category.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
          size.toLowerCase().indexOf(value.toLowerCase()) !== -1
        );
      });
      this.loadService.stop();
    });
  }

  handlDeleteAccount(id: any) {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Xóa sản phẩm',
      message: 'Bạn có muốn xóa sản phẩm này không?',
    };
    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      (data) => {
        if (data) {
          this.loadService.start();
          this.coffeeService.deleteProduct(id).subscribe((result) => {
            this.toastr.success('Thành công!', 'Xóa sản phẩm');
            this.loadAll();
            this.loadService.stop();
          });
        }
      },
      (error) => {
        console.log(error);
        this.toastr.success('Thất bại!', 'Xóa sản phẩm');
      }
    );
  }
  paramsChange(e: any) {
    this.loadService.start();
    this.params.current_page = e.current_page;
    this.loadAll();
    this.loadService.stop();
  }

  findStart() {
    this.start = (this.params.current_page - 1) * this.params.limit;
  }
  findEnd() {
    const result = this.params.current_page * this.params.limit;
    result > this.params.total_records
      ? (this.end = this.params.total_records)
      : (this.end = result);
  }
}
