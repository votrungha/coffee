import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';
import { PRODUCT } from './coffee.model';
import { SERVER_API_URL } from '../../constant';

@Injectable({
  providedIn: 'root',
})
export class CoffeeService {
  constructor(private http: HttpClient) {}
  optionHeader = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  getProduct(req: any): Observable<any> {
    return this.http.get(
      SERVER_API_URL +
        `v1/product/list?name=` +
        req.name +
        '&page=' +
        req.current_page +
        '&size=' +
        req.limit +
        '&reverse=' +
        req.reverse +
        '&sort=' +
        req.sort,
      { observe: 'response' }
    );
  }

  deleteProduct(id: string): Observable<PRODUCT> {
    return this.http.delete<PRODUCT>(
      SERVER_API_URL + `v1/product/delete/${id}`,
      this.optionHeader
    );
  }

  deleteAllProducts(prodects: PRODUCT[]): Observable<any> {
    console.log(prodects);
    return this.http.post<PRODUCT>(
      SERVER_API_URL + `v1/product/deletes`,
      prodects,
      this.optionHeader
    );
  }

  updateProduct(product: PRODUCT, id: string): Observable<PRODUCT[]> {
    return this.http.put<PRODUCT[]>(
      SERVER_API_URL + `v1/product/update/${id}`,
      product,
      this.optionHeader
    );
  }

  getProductById(id: string): Observable<PRODUCT> {
    return this.http.get<PRODUCT>('api/products/' + id).pipe(
      tap((_) => console.log(`fetched product id=${id}`)),
      catchError(this.handleError<PRODUCT>(`getProduct id=${id}`))
    );
  }

  addProduct(product: PRODUCT): Observable<any> {
    return this.http.post(SERVER_API_URL + 'v1/product/insert', product, {
      observe: 'response',
    });
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
