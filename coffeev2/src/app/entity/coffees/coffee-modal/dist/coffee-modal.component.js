"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.CoffeeModalComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var dialog_1 = require("@angular/material/dialog");
var CoffeeModalComponent = /** @class */ (function () {
    function CoffeeModalComponent(data, dialogRef, fb, coffeeService) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.coffeeService = coffeeService;
        this.isValidator = false;
        this.Categorys = ['Cà phê', 'Giải khát', 'Nước suối', 'Sinh tố'];
        this.Sizes = ['M', 'L', 'XL'];
        this.isLoading = false;
        this.ProductForm = this.fb.group({
            name: ['', forms_1.Validators.required],
            id: [''],
            category: ['', forms_1.Validators.required],
            image: ['', forms_1.Validators.required],
            attributes: this.fb.array([
                this.fb.group({
                    size: ['', forms_1.Validators.required],
                    price: ['', forms_1.Validators.required]
                }),
            ]),
            descript: ['', forms_1.Validators.required]
        });
    }
    CoffeeModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        var _a, _b, _c, _d, _e;
        if (this.data.product) {
            (_a = this.ProductForm.get('name')) === null || _a === void 0 ? void 0 : _a.setValue(this.data.product.name);
            (_b = this.ProductForm.get('id')) === null || _b === void 0 ? void 0 : _b.setValue(this.data.product.id);
            (_c = this.ProductForm.get('descript')) === null || _c === void 0 ? void 0 : _c.setValue(this.data.product.description);
            (_d = this.ProductForm.get('image')) === null || _d === void 0 ? void 0 : _d.setValue(this.data.product.image);
            (_e = this.ProductForm.get('category')) === null || _e === void 0 ? void 0 : _e.setValue(this.data.product.category);
            this.attributes.removeAt(0);
            this.data.product.attributes.map(function (item) {
                _this.attributes.push(_this.fb.group({
                    size: item.size,
                    price: item.price
                }));
            });
        }
    };
    Object.defineProperty(CoffeeModalComponent.prototype, "attributes", {
        get: function () {
            return this.ProductForm.get('attributes');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CoffeeModalComponent.prototype, "name", {
        get: function () {
            return this.ProductForm.get('name');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CoffeeModalComponent.prototype, "descript", {
        get: function () {
            return this.ProductForm.get('descript');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CoffeeModalComponent.prototype, "image", {
        get: function () {
            return this.ProductForm.get('image');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CoffeeModalComponent.prototype, "category", {
        get: function () {
            return this.ProductForm.get('category');
        },
        enumerable: false,
        configurable: true
    });
    CoffeeModalComponent.prototype.addAttr = function () {
        this.attributes.push(this.fb.group({
            size: ['', forms_1.Validators.required],
            price: ['', forms_1.Validators.required]
        }));
    };
    CoffeeModalComponent.prototype.deleteAttr = function (index) {
        this.attributes.removeAt(index);
    };
    CoffeeModalComponent.prototype.setValidator = function () {
        this.isValidator = true;
    };
    CoffeeModalComponent.prototype.save = function () {
        this.dialogRef.close(this.ProductForm.value);
    };
    CoffeeModalComponent.prototype.close = function () {
        this.dialogRef.close(null);
    };
    CoffeeModalComponent = __decorate([
        core_1.Component({
            selector: 'app-coffee-modal',
            templateUrl: './coffee-modal.component.html',
            styleUrls: ['./coffee-modal.component.scss']
        }),
        __param(0, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], CoffeeModalComponent);
    return CoffeeModalComponent;
}());
exports.CoffeeModalComponent = CoffeeModalComponent;
