"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MaterialModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var button_1 = require("@angular/material/button");
var icon_1 = require("@angular/material/icon");
var toolbar_1 = require("@angular/material/toolbar");
var list_1 = require("@angular/material/list");
var input_1 = require("@angular/material/input");
var card_1 = require("@angular/material/card");
var table_1 = require("@angular/material/table");
var paginator_1 = require("@angular/material/paginator");
var menu_1 = require("@angular/material/menu");
var checkbox_1 = require("@angular/material/checkbox");
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        core_1.NgModule({
            declarations: [],
            imports: [common_1.CommonModule],
            exports: [
                button_1.MatButtonModule,
                toolbar_1.MatToolbarModule,
                icon_1.MatIconModule,
                list_1.MatListModule,
                input_1.MatInputModule,
                card_1.MatCardModule,
                table_1.MatTableModule,
                paginator_1.MatPaginatorModule,
                menu_1.MatMenuModule,
                checkbox_1.MatCheckboxModule,
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());
exports.MaterialModule = MaterialModule;
