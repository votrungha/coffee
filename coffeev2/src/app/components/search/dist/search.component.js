"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SearchComponent = void 0;
var core_1 = require("@angular/core");
var SearchComponent = /** @class */ (function () {
    function SearchComponent() {
        this.search = new core_1.EventEmitter();
        this.modal = new core_1.EventEmitter();
        this["delete"] = new core_1.EventEmitter();
        this.valueSearch = '';
    }
    SearchComponent.prototype.ngOnInit = function () {
        console.log(this.title);
    };
    SearchComponent.prototype.handlSearch = function (value) {
        this.search.emit(value);
    };
    SearchComponent.prototype.OpenModalAdd = function () {
        this.modal.emit(null);
    };
    SearchComponent.prototype.OpenModalDelete = function () {
        this["delete"].emit();
    };
    __decorate([
        core_1.Output()
    ], SearchComponent.prototype, "search");
    __decorate([
        core_1.Output()
    ], SearchComponent.prototype, "modal");
    __decorate([
        core_1.Input()
    ], SearchComponent.prototype, "title");
    __decorate([
        core_1.Output()
    ], SearchComponent.prototype, "delete");
    __decorate([
        core_1.Input()
    ], SearchComponent.prototype, "params");
    SearchComponent = __decorate([
        core_1.Component({
            selector: 'app-search',
            templateUrl: './search.component.html',
            styleUrls: ['./search.component.scss']
        })
    ], SearchComponent);
    return SearchComponent;
}());
exports.SearchComponent = SearchComponent;
